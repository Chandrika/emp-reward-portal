export interface DashboardInfo {
    empRewards?: number,
    empContributions?: number,
    empName?: string
}