import { FeedInfo } from './feedInfo';

export const getFeedInfo = () => {
    return new Promise((resolve) => {
        const data: FeedInfo[] = [{
            title: 'David G rewarded by John Creet',
            timeStamp: '6hrs ago',
            message: 'Big thanks for your help in helping on the outage today!!'
        },
        {
            title: 'David G rewarded by John Creet',
            timeStamp: 'Feb 1, 2023',
            message: 'Thanks for your help in creating the product overview deck. Your help to showcase these scenarios really helped in closing the loop on the story'
        }];
        resolve(data);
    });
}
