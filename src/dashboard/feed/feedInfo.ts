export interface FeedInfo {
    title?: string;
    timeStamp?: string;
    message?: string;
}