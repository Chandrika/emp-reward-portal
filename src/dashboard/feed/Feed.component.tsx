import { useEffect, useState } from 'react';

import { FeedInfo } from './feedInfo';
import profileIcon from '../../images/profile-icon.jpg';
import { getFeedInfo } from './feed.service';
import './Feed.scss';

const initialFeedState: FeedInfo[] = [];

export function Feed() {

  const [feedInfo, setFeedInfo] = useState(initialFeedState);

  useEffect(() => {
    getFeedInfo().then((val: any) => {
      setFeedInfo(val);
    })
  }, []);

  const renderMessage = (feed: FeedInfo, key: any) => {
    return (
      <div className="message-section" key={key}>
        <div className="msg-icon-div">
          <img src={profileIcon} alt="User Profile" className="msg-icon"></img>
        </div>
        <div className="text-section">
          <div className="txt-title">
            {feed.title}
          </div>
          <div className="txt-timestamp">
            {feed.timeStamp}
          </div>
          <div className="txt-message">
            {feed.message}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="feed-section">
      {
        feedInfo.map((x, i) => (
          renderMessage(x, i)))
      }

    </div>
  );
}
