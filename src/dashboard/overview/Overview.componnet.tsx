import { useEffect, useState } from 'react';

import { DashboardInfo } from '../dashboardInfo.model';
import profileIcon from '../../images/profile-icon.jpg';
import { getDashboardInfo } from '../dashboard.service';
import './Overview.scss';

const initialDashboardState: DashboardInfo = {};

export function Overview() {

  const [dashboardInfo, setDashboardInfo] = useState(initialDashboardState);

  useEffect(() => {
    getDashboardInfo().then((val: any) => {
      setDashboardInfo(val);
    })
  }, []);

  const renderInfoCard = (title: string, amount?: number) => {
    return (
      <div className="info-card">
        <h5 className="info-card-title">{title}</h5>
        <h3 className="info-card-amount">$ {amount}</h3>
      </div>
    );
  }

  return (
    <div className="profile-section">
      <div className="profile-icon-section">
        <img src={profileIcon} alt="User Profile" className="profile-icon"></img>
        <h3>{dashboardInfo.empName}</h3>
      </div>
      {renderInfoCard('Rewards', dashboardInfo.empRewards)}
      {renderInfoCard('Contributions', dashboardInfo.empContributions)}
    </div>
  );
}
