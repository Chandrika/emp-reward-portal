export interface EmpRewardInfo {
    from?: string;
    timeStamp?: string;
    message?: string;
    amount?: number;
}