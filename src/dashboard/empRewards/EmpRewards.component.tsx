import { useEffect, useState } from 'react';

import profileIcon from '../../images/profile-icon.jpg';
import { getEmpRewardsInfo } from './empRewards.service';
import './EmpRewards.scss';
import { EmpRewardInfo } from './empRewardInfo.model';

const initialFeedState: EmpRewardInfo[] = [];

export function EmpRewards() {

  const [feedInfo, setFeedInfo] = useState(initialFeedState);

  useEffect(() => {
    getEmpRewardsInfo().then((val: any) => {
      setFeedInfo(val);
    })
  }, []);

  const renderMessage = (reward: EmpRewardInfo, key: any) => {
    return (
      <div className="message-section" key={key}>
        <div className="msg-icon-div">
          <img src={profileIcon} alt="User Profile" className="msg-icon"></img>
        </div>
        <div className="text-section">
          <div className="txt-title">
            {reward.from}
          </div>
          <div className="txt-timestamp">
            {reward.timeStamp}
          </div>
          <div className="txt-message">
            {reward.message}
          </div>
          <div className="txt-message">
            Amount  - ${reward.amount}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="reward-section">
      {
        feedInfo.map((x, i) => (
          renderMessage(x, i)))
      }

    </div>
  );
}
