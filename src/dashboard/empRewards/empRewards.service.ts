import { EmpRewardInfo } from './empRewardInfo.model';

export const getEmpRewardsInfo = () => {
    return new Promise((resolve) => {
        const data: EmpRewardInfo[] = [{
            from: 'David G',
            timeStamp: 'Jan 15, 2023',
            message: 'Please accept a small token of appreciation for all your hard work and help during the critical hours',
            amount: 200
        },
        {
            from: 'Anni Jest',
            timeStamp: 'Mar 2, 2022',
            message: 'Keep up the good work and appreciate all your efforts. As a token of gratitude sending you a small gift',
            amount: 100
        }];
        resolve(data);
    });
}
