import { DashboardInfo } from './dashboardInfo.model';

export const getDashboardInfo = () => {
    return new Promise((resolve) => {
        const data: DashboardInfo = {
            empRewards: 300,
            empContributions: 400,
            empName: 'Jane Doe'
        }
        resolve(data);
    });
}
