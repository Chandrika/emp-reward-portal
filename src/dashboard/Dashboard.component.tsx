import { useState } from 'react';

import Modal from 'react-bootstrap/Modal';

import { Feed } from './feed/Feed.component';
import { Overview } from './overview/Overview.componnet';
import { NewRewardForm } from './newReward/NewRewardForm.component';
import { EmpRewards } from './empRewards/EmpRewards.component';
import './Dashboard.scss';

const feedTabKey: string = 'feed';
const rewardsTabKey: string = 'myRewards';

export function Dashboard() {

  const [activeTab, setActiveTab] = useState(feedTabKey);
  const [showPopup, setShowPopup] = useState(false);

  const getActiveCSS = (tabKey: string) => {
    return activeTab === tabKey ? 'active' : '';
  }

  const onTabClick = (tabKey: string) => {
    setActiveTab(tabKey);
  }

  const onNewClick = () => {
    setShowPopup(true);
  }

  const onPopupClose = () => {
    setShowPopup(false);
  }

  const onNewRewardFormSubmit = (data: any) => {
    setShowPopup(false);
  }

  const renderNavItem = (tabKey: string, displayText: string) => {
    return (
      <li className="nav-item">
        <button className={`nav-link dashboard-nav-link ${getActiveCSS(tabKey)}`}
          onClick={() => onTabClick(tabKey)}>
          {displayText}
        </button>
      </li>
    );
  }

  const renderTabContent = (tabKey: string, tabIndex: number, tabComponent: JSX.Element) => {
    return (
      <div className={`tab-pane fade show ${getActiveCSS(tabKey)}`}
        id={`${tabKey}-tab-pane`}
        role="tabpanel"
        aria-labelledby={`${tabKey}-tab`}
        tabIndex={tabIndex}>
        <div className="dashboard-tab-content">
          {tabComponent}
        </div>
      </div>
    );
  }

  const renderPopup = () => {
    return (
      <>
        <Modal show={showPopup} onHide={onPopupClose}>
          <Modal.Header closeButton>
            <Modal.Title>New Reward</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <NewRewardForm onFormSubmit={onNewRewardFormSubmit} />
          </Modal.Body>
        </Modal>
      </>
    );
  }

  return (
    <>
      {/* <h2>Dashboard</h2> */}
      <div className="dashboard-div">
        <Overview />
        <div className="btn-new-div">
          <button className="float-right btn btn-primary shadow p-2 mb-7 rounded" onClick={onNewClick}>
            <i></i>
            Give Reward
          </button>
        </div>
        <ul className="nav nav-tabs mb-3">
          {renderNavItem(feedTabKey, 'Feed')}
          {renderNavItem(rewardsTabKey, 'MyRewards')}
        </ul>
        <div className="tab-content" id="dahboardTabContent">
          <>
            {renderTabContent(feedTabKey, 0, <Feed />)}
            {renderTabContent(rewardsTabKey, 1, <EmpRewards />)}
          </>
        </div>
      </div>
      {renderPopup()}
    </>
  );
}
