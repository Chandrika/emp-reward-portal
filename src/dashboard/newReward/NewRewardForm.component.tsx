import React from 'react';
import { useForm } from 'react-hook-form';

import './NewReward.scss';
import { RewardFormProps } from './rewardFormProps.model';

export function NewRewardForm(props: RewardFormProps) {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitted },
  } = useForm({
    mode: "onSubmit"
  });

  const onSubmit = (data: any, event: any) => {
    if (props.onFormSubmit) {
      props.onFormSubmit(data);
    }
    event.target.reset();
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row">
          <div className="col-sm-12 form-control-div">
            <div className="formInput">
              <label className="form-control-label">
                To
                <span className="text-danger">*</span>
              </label>
              <input
                className="form-control"
                {...register("to",
                  {
                    required: true

                  })}
              />
              <span className="text-danger">
                {isSubmitted && errors.to && "To is required"}
              </span>
            </div>
          </div>
          <div className="col-sm-12 form-control-div">
            <div className="formInput">
              <label className="form-control-label">
                Reward<span className="text-danger">*</span>
              </label>
              <input
                type="number"
                step="any"
                min="1"
                className="form-control"
                {
                ...register("reward",
                  {
                    required: true
                  }
                )}
              />
              <span className="text-danger">
                {isSubmitted && errors.reward && "Reward is required"}
              </span>
            </div>
          </div>
          <div className="col-sm-12 form-control-div">
            <div className="formInput">
              <label className="form-control-label">Why?</label>
              <textarea
                className="form-control"
                {...register("comments")}
              />
            </div>
          </div>
          <div className="col-sm-12 form-control-div">
            <div className="d-flex justify-content-center">
              <button type="submit" className="btn btn-primary mt-3 shadow p-2 mb-7 rounded">Reward</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
