export interface RewardFormProps {
    onFormSubmit: (formData: any) => void;
}