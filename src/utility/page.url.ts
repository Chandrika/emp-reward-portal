export class PageUrl {
    public static readonly default: string = '/';
    public static readonly dashboard: string = '/dashboard';
}