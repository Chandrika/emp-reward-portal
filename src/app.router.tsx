import { Route, Routes, Navigate } from 'react-router-dom';

import { PageUrl } from './utility/page.url';
import { Dashboard } from './dashboard/Dashboard.component';

const routes = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to={PageUrl.dashboard} replace />} />
      <Route path={PageUrl.dashboard} element={<Dashboard />} />
      {/* <Route path="*" element={<PageNotFound />} /> */}
    </Routes>
  );
};

export default routes;
