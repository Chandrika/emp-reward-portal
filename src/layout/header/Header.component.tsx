import React from 'react';

import './Header.scss';
import logo from '../../logo.svg';

export function Header() {
    return (
        <header className="d-flex flex-row">
            <img src={logo} className="app-logo" alt="logo" />
            <h1>Rewards Portal</h1>
        </header>
    );
}
