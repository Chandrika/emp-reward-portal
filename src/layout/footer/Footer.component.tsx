import React from 'react';
import './Footer.scss';

export function Footer() {
    return (
        <footer>
            <div className="d-flex flex-row-reverse px-3 py-2">
                All rights reserved ©{(new Date()).getFullYear()}
            </div>
        </footer>
    );
}
