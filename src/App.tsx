import React from 'react';

import './App.scss';

import routes from './app.router';
import { Footer, Header } from './layout';

function App() {
  return (
    <div className="view-page">
      <div className="header-section shadow p-1 mb-3 bg-body rounded">
        <Header />
      </div>
      <div className="route-section">
          {routes()}
      </div>
      <div className="footer-section">
        <Footer />
      </div>
    </div>
  );
}


export default App;
